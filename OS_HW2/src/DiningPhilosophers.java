import java.util.ArrayList;
import java.util.List;


public class DiningPhilosophers {

    public static void main(String[] args) throws Exception {
        final Integer NUM_PHILOSOPHERS = 4;
        int i;
        List<Philosopher> philosophers = new ArrayList<Philosopher>();
        List<Object> forks = new ArrayList<Object>();

        for (i = 0; i < NUM_PHILOSOPHERS; i++) {
            forks.add(new Object());
        }

        for (i = 0; i < NUM_PHILOSOPHERS; i++) {
            Object leftFork = forks.get(i);
            Object rightFork = forks.get((i + 1) % NUM_PHILOSOPHERS);
            Philosopher philosopher;
            //Philosopher with the lowest ID gets to pick the right fork first and then left fork
            //Prevents Deadlock
            if (i == 0) {
                philosopher = new Philosopher(rightFork, leftFork);
            } else {
                philosopher = new Philosopher(leftFork, rightFork);
            }
            Thread t = new Thread(philosopher, "Philosopher " + (i));
            t.start();
        }
    }
}