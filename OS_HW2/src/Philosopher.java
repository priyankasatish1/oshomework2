public class Philosopher implements Runnable {

    Object leftFork, rightFork;
    //Creating forks on either sides of the Philosopher
    public Philosopher(Object leftF, Object rightF) {
        leftFork = leftF;
        rightFork = rightF;
    }
    //Instructs the philosopher to perform an action like "Think", "Eat", and "Sleep"
    public void LogAndSleep(String message) throws InterruptedException {
        System.out.println(System.currentTimeMillis() + " : " + Thread.currentThread().getName() + " : "  + message);
        Thread.sleep((long)(Math.random() * 3000));
        //Thread.sleep((long) 3000);
    }

    public void run() {
        try {
            while(true) {
                LogAndSleep("Think");
                //Use "synchronized keyword to acquire an internal Monitor which simulates picking a fork
                //Prevent other threads from acquiring the fork
                synchronized (leftFork) {
                    LogAndSleep("Pick left fork");
                    synchronized (rightFork) {
                        LogAndSleep( "Pick right fork and eat");
                        LogAndSleep("Put down right fork");
                    }
                    LogAndSleep("Put down left fork. Back to think");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return;
        }
    }
}